#import library pdf2image to convert pdf file to image
from pdf2image import pdfinfo_from_path, convert_from_path
#import tesseract library to extract text from image 
from pytesseract import Output, TesseractError, image_to_string
from pathlib import Path

import re

#Search all pdf files on folder x 
pdf_search = Path(r"where you store all pdf files").glob("*.pdf")

pdf_files = [str(file.absolute()) for file in pdf_search]

#Loop all pdf files
for index, pdf in enumerate(pdf_files):

    print(f"Start convert book number {index + 1} of {len(pdf_files)}")

    info = pdfinfo_from_path(pdf, userpw=None, poppler_path=None)

    maxPages = info["Pages"]

    text = " "

    #Extract text from all pages of pdf file
    for page in range(1, maxPages+1, 10):
        print(f"Converting page number {page} of {maxPages}")

        #Covert all pages of pdf file to images. Process in chunks
        images = convert_from_path(pdf, dpi=200, first_page=page, last_page=min(page+10-1, maxPages))

        for image in images:
            ocr_dict = image_to_string(image, lang='tha', output_type=Output.DICT)
            # ocr_dict now holds all the OCR info including text and location on the image
            txt = ocr_dict['text'].strip()

            text += txt
        print(text)

    name_of_pdf_file = pdf.split("\\")[-1]
    txt_name = re.sub(r'[\\/*?:"<>|]', '', name_of_pdf_file)

    with open(f"thai\\{txt_name}.txt", "w", encoding="utf-8") as f:
        f.write(text)
        print(f"End convert book number {index} of {len(pdf_files)}")

print("Success")